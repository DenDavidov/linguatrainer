export const LOGIN = 'LOGIN';
export const LOGOUT = 'LOGOUT';
export const START = '_START';
export const SUCCESS = '_SUCCESS';
export const ERROR = '_ERROR';