import React, { Component } from 'react';

class Login extends Component {
  state = {
    login: '',
    password: '',
    errorText: null
  };

  _changeLoginHandler(e) {
    e.preventDefault();
    this.setState({
      login: e.target.value
    })
  }

  _changePasswordHandler(e) {
    e.preventDefault();
    this.setState({
      password: e.target.value
    })
  }

  __handleSubmit(e) {
    e.preventDefault();
    this.props.login(this.state);
  }
  
  render() {
    const { error } = this.props;
    return(
      <div>
        <h1>Login</h1>
        <form onSubmit = {this.__handleSubmit.bind(this)}>
          <input
            type = 'text'
            placeholder='login...'
            value = {this.state.login}
            maxLength = {20}
            onChange = {this._changeLoginHandler.bind(this)}
          />
          <input
            type = 'password'
            placeholder='password...'
            value = {this.state.password}
            maxLength = {20}
            onChange = {::this._changePasswordHandler}
          />
          <input type='submit' value='Submit'/>
        </form>
        {(error) ? <p>{error}</p> : null}
      </div>
    )
  }
}

export default Login;