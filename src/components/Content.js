import React, { Component } from 'react';

export default class Content extends Component {

  _logout() {
    this.props.logout();
  }

  render() {
    return(
      <div>
        <h1>Content</h1>
        <p>Lorem Ispum</p>
        <span onClick={::this._logout}>Log out</span>
      </div>
    )
  }
}
