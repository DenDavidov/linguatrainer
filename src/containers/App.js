import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as loginActions from '../actions/user';

import Content from '../components/Content';
import Login from '../components/Login';
import Loader from '../components/Loader';

class App extends Component {
  render() {
    const { user } = this.props;
    const { login, logout } = this.props.loginActions;

    if (user.loading) return <Loader/>;

    if (user.token) {
      return (<Content {...this.props.user} logout={logout}/>)
    } else {
      return (<Login {...this.props.user} login={login}/>)
    }
  }
}

function mapStateToProps (state) {
  return {
    user: state.user
  }
}

function mapDispatchToProps(dispatch) {
  return {
    loginActions: bindActionCreators(loginActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
