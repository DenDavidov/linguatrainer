import {LOGIN, LOGOUT, START, SUCCESS, ERROR} from '../constants/user';

const DELAY = 1000;

export function login(credentials) {
  return (dispatch) => {
    dispatch({
      type: LOGIN+START
    });

    fetch('api/login', {
      method: 'post',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({...credentials})
    })
    .then(function(response) {
      return response.json()
    }).then(function(data) {
      if (data.error) {
        dispatchResult(dispatch, data, ERROR);
      } else {
        dispatchResult(dispatch, data, SUCCESS);
      }
    }).catch(function(ex) {
      dispatchResult(dispatch, {error: 'parsing failed'}, ERROR);
      console.error('parsing failed', ex);
    });
  }
}

function dispatchResult(dispatch, data, result){
  setTimeout(() => {
    dispatch({
      type: LOGIN+result,
      payload: {...data}
    })
  }, DELAY);
}

export function logout() {
  return (dispatch) => {
    dispatch({
      type: LOGOUT+START
    });

    setTimeout(() => {
      dispatch({
        type: LOGOUT+SUCCESS
      })
    }, DELAY);
  }

}