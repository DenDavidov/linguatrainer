import { LOGIN, LOGOUT, START, SUCCESS, ERROR } from '../constants/user';

const initialState = {
  name: null,
  token: null,
  loading: false,
  error: null
};

export default function user(state = initialState, action) {

  switch (action.type) {
    case LOGIN+START:
      return {
        ...state,
        loading: true,
        error: null
      };
    case LOGIN+ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload.error
      };
    case LOGIN+SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
        name: action.payload.name,
        token: action.payload.token
      };
    case LOGOUT+START:
      return {
        ...state,
        loading: true
      };
    case LOGOUT+SUCCESS:
      return {
        ...state,
        loading: false,
        token: null
      };

    default:
      return state;
  }
}
